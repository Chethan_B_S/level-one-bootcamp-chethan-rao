//Write a program to add two user input numbers using one function.
#include<stdio.h>
int main()
{
	float a,b;
	printf("Enter number 1: ");
	scanf("%f",&a);
	printf("Enter number 2: ");
	scanf("%f",&b);
	printf("Sum of %f and %f is %f.",a,b,a+b);
	return 0;
}